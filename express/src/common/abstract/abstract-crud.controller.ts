import { Request, Response, Router } from 'express';
import { AbstractService } from './abstract-crud.service';


export abstract class AbstractCrudController<T> {
	public router = Router();

	protected constructor(protected docService: AbstractService<T>) {
		this.setRoutes();
	}

	public setRoutes() {
		this.router.route('/')
			.get(this.findAll)
			.post(this.create);

		this.router.route('/:id')
			.get(this.find)
			.put(this.update)
			.delete(this.delete);
	}

	protected async findAll(req: Request, res: Response) {
		try {
			res.json(await this.docService.findAll());
		} catch (e) {
			res.status(500).json(e);
		}
	};

	protected async find(req: Request, res: Response) {
		try {
			res.json(await this.docService.find(req.params.id));
		} catch (e) {
			res.status(404).json(e);
		}
	};

	protected async create(req: Request, res: Response) {
		try {
			res.json(await this.docService.create(req.body));
		} catch (e) {
			res.status(404).json(e);
		}
	};

	protected async update(req: Request, res: Response) {
		try {
			const x = await this.docService.update(req.params.id, req.body);
			res.json(x);
		} catch (e) {
			const { message } = e;
			res.status(404).json({ message });
		}
	};

	protected async delete(req: Request, res: Response) {
		try {
			res.json(await this.docService.delete(req.params.id));
		} catch (e) {
			const { message } = e;
			res.status(404).json({ message });
		}
	};

}