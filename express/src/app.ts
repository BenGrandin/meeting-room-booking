import express, { Application, Request, Response } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { RoomController } from './controller/room.controller';
import { RoomService } from './service/room.service';
import { WELCOME_MESSAGE } from './constants/api.constants';

const mongoose = require('mongoose');

class App {
	public app: Application;

	constructor() {
		this.app = express();
		this.setConfig();
		this.connectWithMongo();
		this.setControllers();
	}

	// noinspection JSMethodCanBeStatic
	private connectWithMongo() {
		mongoose.Promise = global.Promise;
		mongoose.connect(process.env.MONGO_URL as string, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false,
		});
		// Renaming _id to id
		mongoose.set('toJSON', {
			virtuals: true,
			transform: (_: any, converted: any) => {
				delete converted._id;
			},
		});
	}

	private setConfig() {
		this.app.use(bodyParser.json({}));
		this.app.use(bodyParser.urlencoded({ extended: true }));
		this.app.use(cors());
	}

	private setControllers() {
		const roomController = new RoomController(new RoomService());
		this.app.get('/', this.sayHello);
		this.app.use('/rooms', roomController.router);
	}

	private sayHello = (req: Request, res: Response) => {
		return res.json(WELCOME_MESSAGE);
	};
}

export default new App().app;