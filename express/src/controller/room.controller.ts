import { Request, Response, Router } from 'express';
import { RoomService } from '../service/room.service';

export class RoomController {
	public router = Router();

	constructor(private roomService: RoomService) {
		this.setRoutes();
	}

	public setRoutes() {
		// TODO : find correct root
		this.router.route('/all')
			.post(this.findAll);

		this.router.route('/')
			.post(this.create);

		this.router.route('/:id')
			.get(this.find)
			.put(this.update)
			.delete(this.delete);
	}


	private findAll = async (req: Request, res: Response) => {
		try {
			console.log(req)
			console.log(req.body)
			const rooms = await this.roomService.findAll(req.body);
			console.log(rooms.map((x: any) => {
				return { 'name': x.name, 'res': x.reservations };
			}));
			res.json(rooms);
		} catch (e) {
			res.status(500).json({ error: e.message });
		}
	};

	private find = async (req: Request, res: Response) => {
		try {
			res.json(await this.roomService.find(req.params.id));
		} catch (e) {
			res.status(404).json(e);
		}
	};

	private create = async (req: Request, res: Response) => {
		try {
			res.json(await this.roomService.create(req.body));
		} catch (e) {
			res.status(404).json(e);
		}
	};

	private update = async (req: Request, res: Response) => {
		try {
			const x = await this.roomService.update(req.params.id, req.body);
			res.json(x);
		} catch (e) {
			const { message } = e;
			res.status(404).json({ message });
		}
	};

	private delete = async (req: Request, res: Response) => {
		try {
			res.json(await this.roomService.delete(req.params.id));
		} catch (e) {
			const { message } = e;
			res.status(404).json({ message });
		}
	};
}